from .event import Event2

class SoignerEvent(Event2):
	NAME="soigner"
	def perform(self):
		if not self.object.has_prop("healer"):
			self.fail()
			return self.inform("soigner.failed")
		self.inform("soigner.avant")
		self.actor.modifierpv(100)
		self.inform("soigner.apres")
