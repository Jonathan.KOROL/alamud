from .event import Event2

class ForceEvent(Event2):
    NAME = "force"

    def perform(self):
        if not self.object.has_prop("deplaçable"):
            self.fail()
            return self.inform("force.failed")
        self.inform("force")
