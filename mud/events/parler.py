from .event import Event2

class ParlerEvent(Event2):
	NAME="parler"
	def perform(self):
		if not self.object.has_prop("speakable"):
			self.fail()
			return self.inform("parler.failed")
		self.inform("parler")
