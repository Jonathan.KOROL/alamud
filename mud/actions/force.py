from .action import Action2
from mud.events import ForceEvent


class ForceAction(Action2):
    EVENT = ForceEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "force"
